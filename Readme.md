 AssecorAssessmentBackendApplication

This Project Made with Spring boot, shell spring, hibernate, Web boot mvc
## Installation
please add the right path to application.proprties like example
(Absolute path)
```bash
spring.datasource.url=jdbc:h2:file:/your path/src/main/resources/data/demo
```
Database will create automatically
## Usage
load sample.csv file using the following command
```shell script
upload /your path/src/main/resources/sample.csv/src/main/resources/sample.csv
```
Now our database is ready to use. 
The available colors are as follow: 
| ID | Farbe |
|---|---|
| 1 | blau |
| 2 | grün |
| 3 | violett |
| 4 | rot |
| 5 | gelb |
| 6 | türkis |
| 7 | weiß |

## API 
**GET** /persons
```json
[{
"id" : 1,
"name" : "Hans",
"lastname": "Müller",
"zipcode" : "67742",
"city" : "Lauterecken",
"color" : "blau"
},{
"id" : 2,
...
}]
```

**GET** /persons/{id}

*Hinweis*: als **ID** kann hier die Zeilennummer verwendet werden.
```json
{
"id" : 1,
"name" : "Hans",
"lastname": "Müller",
"zipcode" : "67742",
"city" : "Lauterecken",
"color" : "blau"
}
```

**GET** /persons/color/{color}
 
*Hinweis*: als **color** kann man beides Ziffern oder Namen benutzen
```json
[{
"id" : 1,
"name" : "Hans",
"lastname": "Müller",
"zipcode" : "67742",
"city" : "Lauterecken",
"color" : "blau"
},{
"id" : 2,
...
}]
```
**POST** /persons/create 
```json
POST /persons/create {"name" : "Hans", "lastname": "Müller", "zipcode" : "67742", "city" : "Lauterecken", "color" : "blau"}
```

I had already written some Junit test 