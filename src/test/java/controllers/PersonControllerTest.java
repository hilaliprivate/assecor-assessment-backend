package controllers;

import AssecorGmbH.assecorassessmentbackend.Colors;
import entities.Person;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import repositories.PersonRepository;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest
public class PersonControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PersonRepository personService;

    @InjectMocks
    private PersonController personController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(personController)
                .build();

    }
    @Test
    public void get_All_Users() throws Exception {
        List<Person> users = Arrays.asList(
                new Person("Ibrahim", "Hilali", "22233", "Berlin", Colors.blau),
                new Person("Maxmann", "Man", "10475", "Berlin", Colors.gelb));
        when(personService.findAll()).thenReturn(users);

        mockMvc.perform(get("/persons"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Ibrahim")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is("Maxmann")))
                .andReturn();
        verify(personService, times(1)).findAll();
        verifyNoMoreInteractions(personService);
    }

    @Test
    public void find_By_Id() throws Exception {
        Person person = new Person("Ibrahim", "Hilali", "22233", "Berlin", Colors.blau);
        when(personService.findById((long) 1)).thenReturn(java.util.Optional.of(person));

        mockMvc.perform(get("/persons/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.aMapWithSize(6)))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasEntry("lastName","Hilali")))
                .andReturn();
        verify(personService, times(1)).findById((long) 1);
        verifyNoMoreInteractions(personService);
    }
    @Test
    public void find_users_with_blau_color() throws Exception {
        List<Person> users = Arrays.asList(
                new Person("Ibrahim", "Hilali", "22233", "Berlin", Colors.blau),
                new Person("Max", "Manno", "10475", "Berlin", Colors.blau));

        when(personService.findByColor(Colors.blau)).thenReturn(users);

        mockMvc.perform(get("/persons/color/blau"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Ibrahim")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is("Max")));
        verify(personService, times(1)).findByColor(Colors.blau);
        verifyNoMoreInteractions(personService);
    }
}