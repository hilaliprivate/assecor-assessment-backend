package controllers;

import AssecorGmbH.assecorassessmentbackend.Colors;
import entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import repositories.PersonRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
public class PersonController {
    private final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/persons")
    public List<Person> index() {
        List<Person> people= personRepository.findAll();
        people.forEach(person -> logger.info(" Repository entities {}",person.toString()));
      return people;
    }

    @GetMapping("/persons/{id}")
    public Person search(@PathVariable("id") Long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if (optionalPerson.isEmpty()) {
            return new Person();
        }
        return optionalPerson.get();
    }

    @GetMapping("/persons/color/{color}")
    public List<Person> searchByColor(@PathVariable("color") String color) {
        if (color.length() < 2) {
            int colorId = Integer.parseInt(color)-1;
            return personRepository.findByColor(Colors.values()[colorId]);
        }
        return personRepository.findByColor(Colors.valueOf(color));
    }

    @PostMapping("/persons/create")
    public String save(@RequestBody Person person) {
        personRepository.save(person);
        logger.info("{} is saved", person.toString());
        return "Done";
    }

    @GetMapping("/values")
    public List<Colors> searchByColor() {
        return Arrays.asList(Colors.values());
    }

}
