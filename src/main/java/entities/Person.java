package entities;

import AssecorGmbH.assecorassessmentbackend.Colors;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;

@Entity
public class Person {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String lastName;
    private String zipcode;
    private String city;
    private Colors color;

    public Person() {

    }

    public Person(String name, String lastName, String zipcode, String city, Colors color) {
        this.name = name;
        this.lastName = lastName;
        this.zipcode = zipcode;
        this.city = city;
        this.color = color;
    }

    @Override
    public String toString() {
        try {
            return (new ObjectMapper()).writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getColor() {
        return color.toString();
    }

    public void setColor(Colors color) {
        this.color = color;
    }
}
