package services;

import AssecorGmbH.assecorassessmentbackend.Colors;
import entities.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CSVService {
    protected static final String ZIPCODE_CITY_EXPRESSION = "([0-9]+)\\s([a-zA-Z]+)";

    protected static final String SEPARATOR = ",";

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<String> failedRecords;

    public CSVService() {
        failedRecords = new LinkedList<>();
    }

    public void clearFailureRegistry() {
        failedRecords.clear();
    }

    public List<String> getFailedRecords() {
        return failedRecords;
    }

    public boolean hasAnyFailedRecords(){
        return failedRecords != null;
    }
    public List<Person> readAndSkippFailedRows(String file) {
        List<Person> People = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int rowNumber = 0;
            while ((line = br.readLine()) != null) {
                rowNumber++;
                String[] values = line.split(SEPARATOR);
                if (values.length < 4) {
                    String failure = String.format(
                            "Row %s with values %s values is corrupted."
                            , rowNumber,
                            Arrays.toString(values)
                    );
                    failedRecords.add(failure);
                    continue;
                }
                String zip = extractZip(values[2]);
                String city = extractCity(values[2]);
                Colors color = extractColor(values[3]);
                Person person = new Person(values[0], values[1], zip, city, color);
                People.add(person);
            }
        } catch (IOException e) {
            logger.error("{}", e.getMessage());
        }
        return People;
    }

    protected Colors extractColor(String value) {
        if (value == null) {
            return null;
        }
        return Colors.values()[Integer.parseInt(value.strip())];
    }

    protected String extractCity(String value) {
        if (value == null) {
            return "";
        }
        Matcher matcher = getMatcher(value);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return "";
    }

    protected String extractZip(String value) {
        if (value == null) {
            return "";
        }
        Matcher matcher = getMatcher(value);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    private Matcher getMatcher(String value) {
        Pattern pattern = Pattern.compile(ZIPCODE_CITY_EXPRESSION);
        return pattern.matcher(value);
    }
}
