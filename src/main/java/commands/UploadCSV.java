package commands;

import entities.Person;
import repositories.PersonRepository;
import services.CSVService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


@ShellComponent
public class UploadCSV {
    protected final Logger logger = LoggerFactory.getLogger(UploadCSV.class);

    protected static final String FILE_NOT_READABLE = "File isn't Readable or does not Exists";
    protected static final String FILE_NOT_UPLOADED = "File(s) did not uploaded";
    protected static final String FILE_UPLOADED = "File(s) uploaded";

    @Autowired
    protected CSVService csvService;

    @Autowired
    protected PersonRepository personRepository;


    @ShellMethod("upload")
    public String upload(@ShellOption("path") String path) {
        if(!Files.isReadable(Path.of(path))){
            return FILE_NOT_READABLE;
        }
        List<Person> people = csvService.readAndSkippFailedRows(path);
        if (people == null) {
            logger.error("There are no readable records.");
            return FILE_NOT_UPLOADED;
        }
        personRepository.saveAll(people);
        if (csvService.hasAnyFailedRecords()) {
            printFailures();
        }
        return FILE_UPLOADED;
    }

    private void printFailures() {
        csvService.getFailedRecords().forEach(failure -> {
            logger.error("Read Failure {}. Record could not read and upload", failure);
        });
        csvService.clearFailureRegistry();
    }
}