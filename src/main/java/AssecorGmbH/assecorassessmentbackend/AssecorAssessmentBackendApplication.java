package AssecorGmbH.assecorassessmentbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;

@SpringBootApplication
@EntityScan("entities")
@ComponentScan("configurations")
@ComponentScan("commands")
@ComponentScan("controllers")
@ComponentScan("services")
@EnableJpaRepositories(basePackages = "repositories")
public class AssecorAssessmentBackendApplication {

	public static void main(String[] args) { SpringApplication.run(AssecorAssessmentBackendApplication.class, args); }

}
